package com.wujunshen.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author: frankwoo(吴峻申) <br>
 * @date: 2017/12/25 <br>
 * @time: 21:21 <br>
 * @mail: frank_wjs@hotmail.com <br>
 */
@FeignClient("test-service")
public interface TestClient {
    @GetMapping("/test")
    void test();
}